Name:    ofono
Summary: Open Source Telephony
Version: 1.31
Release: 1%{?dist}

License: GPLv2
URL:     http://ofono.org/
%undefine _disable_source_fetch
Source0: https://git.kernel.org/pub/scm/network/ofono/ofono.git/snapshot/ofono-%{version}.tar.gz

BuildRequires: ell-devel
BuildRequires: automake libtool
BuildRequires: pkgconfig(glib-2.0)
BuildRequires: pkgconfig(dbus-1)
BuildRequires: pkgconfig(libudev) >= 145
BuildRequires: pkgconfig(bluez)
BuildRequires: pkgconfig(libusb-1.0)
BuildRequires: pkgconfig(mobile-broadband-provider-info)

BuildRequires: systemd
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd

%description
oFono.org is a place to bring developers together around designing an
infrastructure for building mobile telephony (GSM/UMTS) applications.
oFono includes a high-level D-Bus API for use by telephony applications.
oFono also includes a low-level plug-in API for integrating with telephony
stacks, cellular modems and storage back-ends.

%package devel
Summary: Development files for oFono
Requires: %{name} = %{version}-%{release}
%description devel
This package contains files required to develop applications using oFono.


%prep
%setup -q


%build
if [ ! -f configure ]; then
./bootstrap
fi

%configure --enable-external-ell

%make_build


%install
%make_install

# create/own this
mkdir -p %{buildroot}%{_libdir}/ofono/plugins


%check
make check


%post
%systemd_post ofono.service

%preun
%systemd_preun ofono.service

%postun
%systemd_postun_with_restart ofono.service

%files
%doc ChangeLog AUTHORS README
%license COPYING
%{_sysconfdir}/dbus-1/system.d/ofono.conf
%dir %{_sysconfdir}/ofono/
%config(noreplace) %{_sysconfdir}/ofono/phonesim.conf
%{_sbindir}/ofonod
%{_unitdir}/ofono.service
%{_mandir}/man8/ofonod.8*
%dir %{_libdir}/ofono/
%dir %{_libdir}/ofono/plugins/

%files devel
%{_includedir}/ofono/
%{_libdir}/pkgconfig/ofono.pc


%changelog
* Sat Jun 20 2020 Adrian Campos <adriancampos@teachelp.com> - 1.31-1
- First build

